package sheridan;



public class Celsius {
    
    public static int convertFromFahrenheit(int f) {
	int celsius = (int) ((f-32)*(0.5556));
	
	return celsius;
    }
    
    public static void main(String[] args) {
	System.out.println("Celsius: " + Celsius.convertFromFahrenheit(59));
    }

}
