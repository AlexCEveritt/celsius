package sheridan;



import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CelsiusTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testconvertFromFahrenheit() {
	
	assertEquals(0, Celsius.convertFromFahrenheit(32));
	
    }
    
    @Test
    public void testconvertFromFahrenheitNegative() {
	assertNotEquals(0, Celsius.convertFromFahrenheit(56));
    }
    
    @Test
    public void testconvertFromFahrenheitBoundaryIn() {
	assertEquals(-17, Celsius.convertFromFahrenheit(0));
    }

    @Test
    public void testconvertFromFahrenheitBoundaryOut() {
	assertNotEquals(0, Celsius.convertFromFahrenheit(0));
    }
    
}
